class ApplicationController < ActionController::Base
	include Pundit

	protect_from_forgery with: :exception
	respond_to :html, :json
    before_action :configure_permitted_parameters, if: :devise_controller?
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:avatar, :name, :lastName, :email, :username, :password, :birthday, :genre ,:role])
        devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password, :remember_me])
        devise_parameter_sanitizer.permit(:account_update, keys: [:avatar, :name, :lastName, :email, :username, :password, :password_confirmation,  :birthday, :genre,:role, :current_password])
    end

    private

    def user_not_authorized
    	flash[:notice] = "Você não tem autorização para esta ação"
    	redirect_to(request.referrer || root_path)
    end
end
