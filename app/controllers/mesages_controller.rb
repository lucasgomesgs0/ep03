class MesagesController < ApplicationController
  before_action :set_mesage, only: [:show, :edit, :update, :destroy]

  # GET /mesages
  # GET /mesages.json
  def index
    @mesages = Mesage.all
  end

  # GET /mesages/1
  # GET /mesages/1.json
  def show
  end

  # GET /mesages/new
  def new
    @mesage = Mesage.new
    @destUser = params[:destUser]
    @sentUser = params[:sentUser]
  end

  # GET /mesages/1/edit
  def edit
  end

  # POST /mesages
  # POST /mesages.json
  def create
    @mesage = Mesage.new(mesage_params)

    respond_to do |format|
      if @mesage.save
        format.html { redirect_to @mesage, notice: 'Mesage was successfully created.' }
        format.json { render :show, status: :created, location: @mesage }
      else
        format.html { render :new }
        format.json { render json: @mesage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mesages/1
  # PATCH/PUT /mesages/1.json
  def update
    respond_to do |format|
      if @mesage.update(mesage_params)
        format.html { redirect_to @mesage, notice: 'Mesage was successfully updated.' }
        format.json { render :show, status: :ok, location: @mesage }
      else
        format.html { render :edit }
        format.json { render json: @mesage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mesages/1
  # DELETE /mesages/1.json
  def destroy
    @mesage.destroy
    respond_to do |format|
      format.html { redirect_to mesages_url, notice: 'Mesage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mesage
      @mesage = Mesage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mesage_params
      params.require(:mesage).permit(:mesage, :sentUser_id, :destUser_id, :dataSent, :timeSent)
    end
end
