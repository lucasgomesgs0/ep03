json.extract! post, :id, :post, :date, :time, :user_id, :created_at, :updated_at
json.url post_url(post, format: :json)
