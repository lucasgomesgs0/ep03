class CreateMesages < ActiveRecord::Migration[5.2]
  def change
    create_table :mesages do |t|
      t.string :mesage
      t.references :sentUser, foreign_key: true
      t.references :destUser, foreign_key: true
      t.date :dataSent
      t.time :timeSent

      t.timestamps
    end
  end
end
