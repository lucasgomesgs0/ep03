require "application_system_test_case"

class MesagesTest < ApplicationSystemTestCase
  setup do
    @mesage = mesages(:one)
  end

  test "visiting the index" do
    visit mesages_url
    assert_selector "h1", text: "Mesages"
  end

  test "creating a Mesage" do
    visit mesages_url
    click_on "New Mesage"

    fill_in "Datasent", with: @mesage.dataSent
    fill_in "Destuser", with: @mesage.destUser_id
    fill_in "Mesage", with: @mesage.mesage
    fill_in "Sentuser", with: @mesage.sentUser_id
    fill_in "Timesent", with: @mesage.timeSent
    click_on "Create Mesage"

    assert_text "Mesage was successfully created"
    click_on "Back"
  end

  test "updating a Mesage" do
    visit mesages_url
    click_on "Edit", match: :first

    fill_in "Datasent", with: @mesage.dataSent
    fill_in "Destuser", with: @mesage.destUser_id
    fill_in "Mesage", with: @mesage.mesage
    fill_in "Sentuser", with: @mesage.sentUser_id
    fill_in "Timesent", with: @mesage.timeSent
    click_on "Update Mesage"

    assert_text "Mesage was successfully updated"
    click_on "Back"
  end

  test "destroying a Mesage" do
    visit mesages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Mesage was successfully destroyed"
  end
end
